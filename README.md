
## Simple library application

This is an awesome library application for managing a small library of different items.
Unfortunately our developer could not finalize the application before he resigned and we would like you to give the final touch.

### 1. Fix unit test's
Start by running following unit test's, you'll see that test's are failing:

* fi.op.test.library.shelf.book.BookshelfTest

See what it needs to make those test's to pass.

### 2. Fix spring configuration (toteutus ja confis puuttuu vielä...)
Try to run the Springboot application:
```
mvn clean install spring-boot:run
``` 
I'am not sure but there might be some spring configuration issue preventing the application from starting.
See if you can figure out the problem. It could have something to do with dependency injection...


## Run the Springboot application

To make the final test you should run the Springboot application and access it in the browser to confirm that your magic did make a difference.

Application should be accessible from url: [http://localhost:8081/library] 