package fi.op.test.library.item;

import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Book - implements {@link Comparable} interface for easier class {@link Comparable natural ordering}.
 *
 * @version 1.0
 */
public class Book implements LibraryItem, Comparable<Book> {
    private String title;
    private String author;

    public Book(String title, String author) {
        super();
        this.title = title;
        this.author = author;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return new EqualsBuilder().append(hashCode(), obj.hashCode()).isEquals();
    }

    @Override
    public int compareTo(Book other) {
        return this.equals(other) ? 0 : this.title.compareTo(other.getTitle());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Book [title=").append(title).append(",").append("author").append(author).append("]");
        return builder.toString();
    }

}
