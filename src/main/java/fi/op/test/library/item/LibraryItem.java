package fi.op.test.library.item;

/**
 * LibraryItem interface
 *
 * @version 1.0
 */
public interface LibraryItem {
    /**
     * Return the title of the item
     *
     * @return
     */
    String getTitle();
}
