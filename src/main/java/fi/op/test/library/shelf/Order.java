package fi.op.test.library.shelf;

/**
 * Enum defining order direction
 *
 * @version 1.0
 */
public enum Order {
    ORDER_ASCENDING, ORDER_DESCENDING;
}
