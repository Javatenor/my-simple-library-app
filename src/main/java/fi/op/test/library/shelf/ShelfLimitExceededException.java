package fi.op.test.library.shelf;

/**
 * LimitExceededException
 *
 * @version 1.0
 */
public class ShelfLimitExceededException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ShelfLimitExceededException(String message) {
        super(message);
    }
}
