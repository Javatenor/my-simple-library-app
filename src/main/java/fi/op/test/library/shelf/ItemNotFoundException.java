package fi.op.test.library.shelf;

/**
 * ItemNotFoundException
 *
 * @version 1.0
 */
public class ItemNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ItemNotFoundException(String message) {
        super(message);
    }
}