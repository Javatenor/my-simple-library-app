package fi.op.test.library.shelf.book;

import java.util.LinkedList;
import java.util.List;

import fi.op.test.library.item.Book;
import fi.op.test.library.shelf.Order;
import fi.op.test.library.shelf.Shelf;

/**
 * Bookself
 *
 * @version 1.0
 */
public class Bookshelf implements Shelf<Book> {
    /** maximum amount of books in the shelf */
    private int maxLimit;
    /** list of books in the shelf */
    private List<Book> books = new LinkedList<>();

    public Bookshelf(int maxLimit) {
        this.maxLimit = maxLimit;
    }

    @Override
    public List<Book> getItems(Order order) {
        // TODO ADD YOUR CODE TO SORT BOOKS IN DEFINED ORDER
        // ...
        return books;
    }

    @Override
    public void removeItem(Book book) {
        // TODO ADD YOUR CODE TO REMOVE A BOOK FROM THE LIST AND THROW ItemNotFoundException IF ITEM IS NOT FOUND
        // ...

    }

    @Override
    public void addItem(Book book) {
        // TODO ADD YOUR CODE TO THROW ShelfLimitExceededException IF SHELF MAX LIMIT IS EXCEEDED
        // ...
        books.add(book);
    }



}
