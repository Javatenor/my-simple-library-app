package fi.op.test.library.shelf;

import java.util.List;

import fi.op.test.library.item.LibraryItem;

/**
 * Shelf interface
 *
 * @version 1.0
 * @param <T>
 */
public interface Shelf<T extends LibraryItem> {
    /**
     * Returns item in the Self in given order
     *
     * @param order
     * @return
     */
    List<T> getItems(Order order);

    /**
     * Remove item from the Shelf
     *
     * @param item
     */
    void removeItem(T item);

    /**
     * Add an item into the Shelf
     *
     * @param item
     */
    void addItem(T item);

}
