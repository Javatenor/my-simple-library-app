package fi.op.test.library.shelf.book;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import fi.op.test.library.item.Book;
import fi.op.test.library.shelf.ItemNotFoundException;
import fi.op.test.library.shelf.Order;
import fi.op.test.library.shelf.Shelf;
import fi.op.test.library.shelf.ShelfLimitExceededException;

/**
 * Unit test's to test that Bookshelf is behaving like it should
 *
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
public class BookshelfTest {

    Shelf<Book> sut; // service under test

    @BeforeEach
    public void setup() {
        // initialize Bookself with 3 books and limit of 4
        sut = new Bookshelf(4);
        sut.addItem(new Book("Thinking In Java", "Bruce Eckel"));
        sut.addItem(new Book("Head First Java", "Kathy Sierra"));
        sut.addItem(new Book("Head First design patterns", "Kathy Sierra"));
    }

    /**
     * Test that returned item list is ordered accordingly
     */
    @Test
    public void shouldHaveAlphabeticallyCorrectOrder() {
        // assert that alphabetically first Book is returned when ordered ascending
        assertThat(sut.getItems(Order.ORDER_ASCENDING).get(0).getTitle()).isEqualTo("Head First Java");

        // assert that alphabetically last Book is returned when ordered descending
        assertThat(sut.getItems(Order.ORDER_DESCENDING).get(0).getTitle()).isEqualTo("Thinking In Java");
    }

    /**
     * Test that {@link ShelfLimitExceededException} is thrown when Shelf limit is exceeded
     */
    @Test
    public void shouldThrowException_whenShelfLimitExceeded() {
        sut.addItem(new Book("Bitter Java", "Bruce Tate"));

        Assertions.assertThrows(ShelfLimitExceededException.class, () -> {
            // adding a fourth Book into shelf should exceed the shelf limit (4)
            sut.addItem(new Book("Java design patterns", "Bruce Tate"));
        }, "Should have thrown exception");

    }

    /**
     * Test that {@link ItemNotFoundException} is thrown when book to be removed is not found
     */
    @Test
    public void shouldThrowException_whenRemovedItemNotFound() {

        Assertions.assertThrows(ItemNotFoundException.class, () -> {
            // removing a Book that is not in the Shelf should throw exception
            sut.removeItem(new Book("Not found title", "Bruce Tate"));
        }, "Should have thrown exception");

    }
}
